# Prova prática - DSIII

Este é um projeto criado exclusivamente para a prova prática de DSIII. O projeto consiste em um Blog hipotético onde o aluno deve efetuar as modificações listadas abaixo:

- Adicionar um sistema de comentários, isto é, um sistema utilizado em blogs para que pessoas possam escrever suas opiniões sobre um determinado post. Para isso será necessário:
  - Criar uma *model* conforme a modelagem do banco de dados abaixo;
  ![](modelo-bd.png)
  - Criar uma *controller* que execute um CRUD nos dados da tabela, e que esteja protegida para que somente seja acessada por usuários logados (adminsitradores);
  - Ajustar as *views* da *HomeController* que exibem somente os posts, adicionando um formulário de cadastro para os comentários e abaixo exibir todos os comentários já cadastrados para este post.
- Traduzir botões, links e o maior número de mensagens possíveis para a linguagem português brasileiro.

É permitido utilizar todos os conhecimentos de CSS, JavaScript e HTML para auxiliar na prova. É **obrigatório** seguir os padrões visuais do [Bootstrap](https://getbootstrap.com/).

Boa prova!
