using System;

namespace BlogProva.Models
{
    public class Post
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public DateTime Data { get; set; }
        public string Conteudo { get; set; }
        public string Autor { get; set; }
    }
}
